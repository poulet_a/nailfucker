NailFucker
======

You will lose your money as much as you eat them !!  
By Sopheny  

1. Client Usage
2. Server Usage
3. Protocol Specifications
4. Configuration Specifications (Server)


1. Client Usage
------
Connect to a server with :  
``./client.rb <-u USERNAME> <-p PASSWORD> <-ip IP.IP.IP.IP> <-port PORT> ACTION``  
__Note : If no ip is specified, it will be ask again__  
__Note : The list of all availiable ACTION can be found with the command 'help'__  
__Note : You should have a correct version to stay compatible with the server__  


2. Server Usage
------
You can run the server with  
``./server <-p PORT> <-l BROADCAST>``  


3. Protocol Specifications
------

Protocol :  
```
> server : motd
> client : (username)  
> client : (password)  
> server : "succes"/"failure"  
		> client : (cmd)  
		> server : (output)/"failure"
		> server : ">end<"
```

0. The server send the modt (and a ">end<", see #5)
1. The client send the username
2. The client send the password
3. The server answers "succes" or "failure"
4. The client send a cmd
5. The server send the output

	The server output will seem is like :  
	```
	> text
	> text 2
	> datatada
	> bin
	> text 3
	> >end<
	```
	The ">end<" means the server finished the output&waits for a new cmd.  


4. Configuration Specifications (Server)
------
The configuration file must be presented as :
```
key1=valueX
key2=valueY
```

__List of all key and values__  :
key | values | description
--------------------------
registration | "true"/"false" | open or close the account registration from client
adpin | key.sha2 | the admin password (sha2 encrypted)

You can change the default configuration at ~/.config/NailFucker/config.cfg  
You also can write in the terminal server your configuration by "key=value"  
