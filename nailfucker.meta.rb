module NailFucker

  require 'version'

  META = {
    version:            "2.9b".to_version,
    version_long:       "version 2.9 Beta",
    version_protocol:   "1.0.1".to_version,
    version_protocol_compatible:   "1.0.1".to_version,
    author:             "Sopheny"
  }

  INFOS = "NailFucker #{NailFucker::META[:version_long]} \
(protocol #{NailFucker::META[:version_protocol]}) \
by #{NailFucker::META[:author]}"


end
