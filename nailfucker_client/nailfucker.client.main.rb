#encoding: utf-8

require 'socket'
require 'rubyhelper'
require_relative 'nailfucker.client.execute.rb'
require_relative 'nailfucker.client.modt.rb'

module NailFucker
  class Client

    DEFAULT_IP   = '127.0.0.1'
    DEFAULT_PORT = 4202

    def host
      return @ip.to_s + ':' + @port.to_s
    end

    def get_username
      while @username == nil or @username.size < 1
        print "Username for " + self.host + " > "
        @username = STDIN.gets.chomp.to_s
      end
      @username = @username.to_s.chomp
    end

    def get_password
      while @password == nil or @password.size < 1
        print "Password for " + @username + "@" + self.host + " > "
        @password = STDIN.gets.chomp.to_s
      end
      @password = @password.to_s.chomp
    end

    def initialize username=nil, password=nil, ip=DEFAULT_IP, port=DEFAULT_PORT, cmds=[]
      @ip = ip || DEFAULT_IP #default value on NIL TOO
      @port = port || DEFAULT_PORT
      @username = username
      @password = password
      @cmds = cmds.map{|cmd| Client::tr_cmd(cmd)}
      trap("SIGINT"){
        puts "\nQuit"
        @s.puts "exit" if defined?@s
        exit(0)
      }
    end

    def get_messages
      out = Array.new
      line = ">beg<"
      while line = @s.gets.to_s.chomp and line != ">end<"
        out << line
      end
      return out
    end

    def connect
      puts "Connection to " + self.host + "..."

      begin
        @s = TCPSocket.open(@ip, @port)
      rescue
        puts "Connection failed !"
        return nil
      end
      puts "Connection etablished !"
      puts
      start()
    end

    private
    def start
      get_modt()
      get_username()
      get_password()
      #send the username, then the password, then wait for answer
      @s.puts @username
      @s.puts @password.sha2
      #puts infos
      puts "Login&Password sent !"
      puts "Waiting for answer..."
      #send succes or failed
      if @s.gets.to_s.chomp == "succes"
        puts "Login succes !"
        puts
        if @cmds.size == 0
          interactive()
        else
          execute_cmds()
        end
      else
        puts "Login failed !"
        puts
        #self.close()
      end
    end

    def close
      @s.close
    end

    def interactive
      cmd = ""
      while cmd != "quit" and cmd != "exit" do
        print "Command > "
        cmd = STDIN.gets.to_s.chomp
        next if cmd.split.size < 1
        execute_cmd(cmd)
        puts
      end
    end

  end
end
