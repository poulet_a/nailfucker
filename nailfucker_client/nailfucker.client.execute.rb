#encoding: utf-8

require 'rubyhelper'

module NailFucker
  class Client

    # Parse an user cmd to a readable cmd
    def self.tr_cmd cmd_in
      cmd_out = cmd_in.to_s
      #Parse the commit
      if cmd_out.match(/\A(\-|\+)?(\d)+\Z/)
        cmd_out = "commit " + cmd_out
        #sha2 mdp
      elsif mdp = cmd_out.match(/\A(chgpasswd )(\w+)\Z/)
        cmd_out = "chgpasswd " + mdp[2].to_s.sha2
      elsif mdp = cmd_out.match(/\A(logon_admin )(\w+)\Z/)
        cmd_out = "logon_admin " + mdp[2].to_s.sha2
      end
      return cmd_out
    end

    def execute_cmd_handle_result(res)
      if res.match(/\Acount=.+\Z/)
        puts "Your account is " + (res.split("=")[1].to_f / 100).round(2).to_s + "€"
      elsif res == "exit"
        puts "End of Stream. Thanks you for visiting."
        exit(0)
      else
        puts "|" + res
      end
    end

    def execute_cmd cmd
      cmd = NailFucker::Client.tr_cmd(cmd)
      @s.puts(cmd)
      #Handle the result
      #While res != >end< continue to read
      results = get_messages
      results.each do |res|
        execute_cmd_handle_result(res)
      end
      return true
    end

    #Executes the @cmds
    private
    def execute_cmds
      cmds = @cmds if cmds == nil
      @cmds.each do |cmd|
        execute_cmd(cmd)
      end
      @s.puts ">end<"
      return true
    end

  end
end
