#!/usr/bin/env ruby
#encoding: utf-8

require_relative 'nailfucker.meta.rb'
require_relative 'nailfucker_server/nailfucker.server.main.rb'

def get_hash argv
  i = 0
  h = {
    username: nil,
    password: nil,
    ip: '127.0.0.1',
    port: 4202,
    cmds: []
  }

  while i < argv.size
    if argv[i].match(/\A-p(ort)?\Z/)
      i += 1
      h[:port] = argv[i]
    elsif argv[i].match(/\A(-l(isten)?)|(-ip)\Z/)
      i += 1
      h[:listen] = argv[i]
    end
    i += 1
  end
  return h
end

if RUBY_VERSION.to_version < "2.0".to_version
  puts "Your ruby version is not greater or equal than 2.0"
  exit(0)
end

puts "Server " + NailFucker::INFOS
puts

h = get_hash(ARGV)
s = NailFucker::Server.new(h[:listen], h[:port])
s.start
