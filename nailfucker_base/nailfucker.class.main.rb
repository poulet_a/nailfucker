#encoding: utf-8

require_relative 'nailfucker.class.log.rb'
require_relative 'nailfucker.class.files.rb'
require_relative 'nailfucker.class.consts.rb'
require_relative 'nailfucker.class.account.rb'
require_relative 'nailfucker.class.base_files.rb'

module NailFucker
  class Base

    # No config yet
    @@config = {}

    def self.config
      return @@config
    end

  end
end
