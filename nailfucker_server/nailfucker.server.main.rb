#encoding: utf-8

require 'socket'
require 'rubyhelper'

require_relative 'nailfucker.server.execute.rb'
require_relative 'nailfucker.server.cmds_local.rb'
require_relative 'nailfucker.server.cmds_client.rb'
require_relative 'nailfucker.server.log.rb'
require_relative 'nailfucker.server.config.rb'
require_relative '../nailfucker_base/nailfucker.class.main.rb'

module NailFucker
  class Server

    def initialize listen="0.0.0.0", port=4202, modt="Welcome", config={}
      @listen = listen.to_s
      @port = port.to_i
      @config = {}
      config_load!.merge!(config)
      @modt = modt
      begin
        @s = TCPServer.new @listen, @port
        log LOG_SERV, "Serveur running"
        trap("SIGINT"){ log LOG_SERV, "[SIGINT] Serveur exit" ; exit(0) }
      rescue
        log LOG_SERV, "Serveur failed"
        return nil
      end
    end

    def start
      return (log LOG_SERV, "No Stream found...") if !@s
      Thread.start{ interactive() }
      loop do
        Thread.start(@s.accept) do |client|
          begin
            handle_client(client)
          rescue
            log LOG_SERV, "Client failed"
          end
          client.close
          log LOG_SERV, "Client died"
        end
      end
    end

    def interactive
      loop do
        print "\n$> "
        cmd = STDIN.gets.to_s.chomp
        next if cmd == ""
        execute_cmd_local(cmd)
      end
    end

    def send_modt client
      client.puts "version_protocol:" + NailFucker::META[:version_protocol].to_s
      client.puts "version_server:" + NailFucker::META[:version].to_s
      client.puts @modt.to_s
      client.puts ">end<"
    end

    def handle_client client
      log LOG_SERV, "New client connected"

      send_modt(client)

      username = client.gets.to_s.chomp
      password = client.gets.to_s.chomp.sha2

      user = Account.find_by_username(username)
      user = Account.new(id: Account.all.keys.max.to_i + 1,
                         username: username,
                         password: password,
                         count: 0) if !user and @config[:registration] # register a new account
      if user and user.password == password
        log user.id, user.username + " is connected from (" + client.addr.last + ")"
        client.puts "succes"
        user.save! # save the user on connection
      else
        log user.id, "connection failure from " + client.addr.last if user
        client.puts "failure"
        client.puts ">end<"
      end

      cmd = ">begin<"
      while cmd != ">end<"
        cmd = client.gets.to_s.chomp
        execute_cmd_client(client, user, cmd)
      end
    end

  end
end
