#encoding: utf-8

require 'rubyhelper'

module NailFucker
  class Server

    COMMANDS_CLIENT = [
      :admin,
      :chgpasswd,
      :clearlog,
      :commit,
      :count,
      :exit,
      :help,
      :log,
      :logon_admin,
      :quit
    ]

    COMMANDS_CLIENT_DESCR = {
      admin: "<admin CMD [PARAMS]> : execute a admin cmd",
      chgpasswd: "<chgpasswd NEWPASSWD> : change your password to an other. It will be encrypted",
      clearlog: "<clearlog> : Remove your logs",
      commit: "<commit +/-AMOUNT> : do a changement on your account (+X, -X). You can also write \"+X\"",
      count: "<count> : display how much money you have",
      exit: "<exit> : exit",
      help: "<help> : Display this",
      log: "<log [N=10]> : get the N last log lines of your account (Default N=10)",
      logon_admin: "<logon_admin ADMIN_MDP> : get the admin rights",
      quit: "<quit> : exit"
    }

    def cmd_client_admin user, cmd
      if user.admin == true
        cmd = cmd.split[1..-1].join(" ")
        log user.id, "admin" + cmd
        out = execute_cmd_local cmd
        return out
      else
        return "You are not an administrator. Please user logon_admin."
      end
    end

    def cmd_client_exit user, cmd
      log user.id, user.username + " left"
      return "exit"
    end

    def cmd_client_quit user, cmd
      return cmd_client_exit user, cmd
    end

    def cmd_client_count user, cmd
      return "count=" + user.count.to_i.sign + user.count.to_i.abs.to_s
    end

    def cmd_client_chgpasswd user, cmd
      #puts "actual " + user.password
      #puts "new " + cmd.split[1].to_s.sha2
      user.password = cmd.split[1].to_s.sha2
      log user.id, user.username + " changed his password by " + user.password + " (" + cmd.split[1].to_s + ")"
      user.save!
      return "succes"
    end

    def cmd_client_commit user, cmd
      user.count = user.count + cmd.match(/(\+|\-)?(\d)+/).to_s.to_i
      log user.id, user.username + " updated his account : " + user.count.to_s + "(" + cmd + ")"
      user.save!
      return cmd_client_count(user, cmd)
    end

    def cmd_client_log user, cmd
      f = NailFucker::Base.open_log('r')
      n = 10
      n = cmd.to_s.split[1].to_s.to_i.abs if cmd.to_s.split.size == 2
      lines = []
      while line = f.gets do
        if line.to_s.split("::")[1].to_s.to_i == user.id
          lines << line.chomp
        end
      end
      f.close
      return lines[(lines.size < n) ? (0) : (lines.size - n), lines.size].join("\n")
    end

    def cmd_client_logon_admin user, cmd
      if @config[:admin]
        mdp = cmd.split[1].to_s
        if mdp.sha2 == @config[:admin]
          user.admin = true
          log user.id, user.username + " logon as admin"
          return "login successfull"
        else
          return "login failed"
        end
      else
        return "the server does not support admin logon"
      end
    end

    def cmd_client_clearlog user, cmd
      return "Not implemeted yet"
    end

    def cmd_client_help user, cmd
      out = []
      COMMANDS_CLIENT_DESCR.each do |k, v|
        out << v.to_s
      end
      return out.join("\n")
    end

  end
end
