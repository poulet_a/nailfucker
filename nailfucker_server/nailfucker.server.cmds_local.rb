#encoding: utf-8

module NailFucker
  class Server

    COMMANDS_LOCAL  = [
      :accountlist,
      :clearlog,
      :config,
      :configlist,
      :configreload,
      :exit,
      :help,
      :log,
      :reload
    ]

    COMMANDS_LOCAL_DESCR = {
      accountlist: "<accountlist> : list all accounts",
      clearlog: "<clearlog> : Remove the logs",
      config: "<config> : configlist's alias",
      configlist: "<configlist> : list all config parameters",
      configreload: "<configreload> : reload the configuration file",
      exit: "<exit> : close the server",
      help: "<help> : Display this",
      log: "<log [N=40]> : Get the N last log lines",
      reload: "<reload> : configreload's alias"
    }

    def cmd_local_config cmd
      return cmd_local_configlist(cmd)
    end

    def cmd_local_configlist cmd
      out = []
      @config.each do |k, v|
        puts (out << k.to_s + "=" + v.to_s).last
      end
      return out.join("\n")
    end

    def cmd_local_configreload cmd
      config_reset!
      config_load!
      log LOG_SERV, "Server reloaded"
      return "Server reloaded"
    end

    def cmd_local_exit cmd
      log LOG_SERV, "Serveur exit"
      exit(0)
    end

    def cmd_local_help cmd
      out = []
      COMMANDS_LOCAL_DESCR.each do |k, v|
        puts (out << v.to_s).last
      end
      return out.join("\n")
    end

    def cmd_local_log cmd
      out = []
      f = NailFucker::Base.open_log('r')
      n = 40
      n = cmd.to_s.split[1].to_s.to_i.abs if cmd.to_s.split.size == 2
      while line = f.gets do
        puts (out << line.chomp).last
      end
      f.close
      return out.last(n).join("\n")
    end

    def cmd_local_accountlist cmd
      out = []
      all = NailFucker::Account.all
      #all.short
      all.each do |id, a|
        puts (out << a.to_s).last
      end
      return out.join("\n")
    end

    def cmd_local_clearlog cmd
      f = NailFucker::Base.open_log('w')
      f.close
      log LOG_SERV, "Reset log"
      return "Reset log"
    end

    def cmd_local_reload cmd
      return cmd_local_configreload cmd
    end

  end
end
